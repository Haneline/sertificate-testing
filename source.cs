using System;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.IO;

namespace MainFile
{
  
    class Program
    {
        static void Main(string[] args)
        {
            string searchingName = "Ann"; //искомое имя сертификата
            X509Store x509Store = new X509Store(StoreName.My,StoreLocation.CurrentUser);
            //хранилище сертификатов, указывает на локального пользователя и подхранилище "личное"
            x509Store.Open(OpenFlags.ReadWrite); //открываем хранилище с правами для чтения и записи
            foreach (var onecert in x509Store.Certificates)
            {
                Regex regex = new Regex(@"CN="+searchingName);
                if (regex.IsMatch(onecert.Issuer))//отлавливаем нужный сертификат по регулярному выражению
                { 
                    Console.WriteLine("Найден");
                    File.WriteAllBytes(@"wow.pfx", onecert.Export(X509ContentType.Pfx, "password1234"));
                    //экспортируем в pfx с нужным паролем и по относительному пути с именем wow.pfx
                }
            }
        }
    }
}